from setuptools import setup, find_packages


if __name__ == '__main__':
    package_name = 'map_reduce_udacity'
    setup(
        name=package_name,
        author='Samuel Marks',
        version='0.4.2',
        test_suite=package_name + '.tests',
        packages=find_packages(),
        package_dir={package_name: package_name},
        package_data={'data': ['*']}
    )
