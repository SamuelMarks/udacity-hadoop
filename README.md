Python project for [Udacity's: "Intro to Hadoop and MapReduce"](https://www.udacity.com/course/intro-to-hadoop-and-mapreduce--ud617)

## Minimum requirements
Tested with `Python 2.7.*` on Windows and Linux, with `pip 1.4.*` and `pip 1.5.*`.

## Install
Install project with:

    pip install .

## Run test suite

    python setup.py test
