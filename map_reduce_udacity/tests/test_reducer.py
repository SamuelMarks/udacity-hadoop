from unittest import TestCase, main as unittest_main
from collections import Counter
from cStringIO import StringIO

from map_reduce_udacity.experiments.total_sales_per_category import total_sales_per_category


class TestReducer(TestCase):
    """
    StringIO('\n'.join('\t'.join(i)
                  for i in [['Toys', '500'], ['Toys', '430'],
                            ['Consumer Electronics', '300'],
                            ['Consumer Electronics', '240']])),
    """

    def test_reducer(self):
        self.assertEqual(
            reduce(total_sales_per_category,
                   [['Toys', '500'], ['Toys', '430'], ['Consumer Electronics', '300'], ['Consumer Electronics', '240']],
                   Counter()),
            Counter({'Toys': 930, 'Consumer Electronics': 540})
        )


if __name__ == '__main__':
    unittest_main()
