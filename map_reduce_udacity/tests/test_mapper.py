from unittest import TestCase, main as unittest_main
from StringIO import StringIO

from map_reduce_udacity.mappers import mapper
from map_reduce_udacity.utils import to_tsv, retrieve_file, retrieve_io_object
from csv import reader as csv_reader, writer as csv_writer


class TestMapper(TestCase):
    def test_tsv_line(self):
        filter_by_headers = 'category', 'price'

        res = mapper(
            args=StringIO(to_tsv([['2012-01-01', '09:00', 'San Jose', "Men's Clothing", '214.05', 'Amex'],
                                  ['2012-01-01', '09:00', 'Fort Worth', "Women's Clothing", '153.57', 'Visa']])),
            headers=('date', 'time', 'store', 'category', 'price', 'card'), filters=filter_by_headers)
        self.assertEqual(res.values, [("Men's Clothing", '214.05'), ("Women's Clothing", '153.57')])

        print to_tsv(res.values)

    def test_clf_line(self):
        filter_by_headers = 'request', 'status'

        res = mapper(
            args=StringIO(to_tsv([
                ['10.223.157.186', '-', '-', '15/Jul/2009:14:58:59 -0700', 'GET / HTTP/1.1', '403', '202'],
                ['10.223.157.186', '-', '-', '15/Jul/2009:14:59:03 -0700', 'GET /foo HTTP/1.1', '200', '202']
            ])),
            headers=('ip', 'identity', 'username', 'timestamp', 'request', 'status', 'bytes'),
            filters=filter_by_headers)
        self.assertEqual(res.values, [('GET / HTTP/1.1', '403'), ('GET /foo HTTP/1.1', '200')])

        print to_tsv(res.values)

    def test_line_count(self):
        filename = 'head_50_forum_node.tsv'
        filter_by_headers = 'body',

        mine = mapper(args=retrieve_file(filename), headers_included=True, filters=filter_by_headers)
        with retrieve_io_object(filename) as f:
            reader = csv_reader(f, delimiter='\t', quotechar='"')
            header = reader.next()
            std = [row[4] for row in reader]

        self.assertEqual(len(mine.values), len(std))
        self.assertEqual(mine.values, std)


if __name__ == '__main__':
    unittest_main()
