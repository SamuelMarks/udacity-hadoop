from unittest import TestCase, main as unittest_main
from map_reduce_udacity.utils import pick_col


class TestPick(TestCase):
    def test_no_val(self):
        self.assertEqual(
            pick_col(('foo', 'bar', 'can'), ('dummy0', 'dummy1', 'dummy2')),
            ('foo', 'bar', 'can')
        )

    def test_one_val(self):
        self.assertEqual(
            pick_col(('foo', 'bar', 'can'), ('dummy0', 'dummy1', 'dummy2'), ('dummy0',)),
            ('foo',)
        )

    def test_two_vals(self):
        self.assertEqual(
            pick_col(('foo', 'bar', 'can'), ('dummy0', 'dummy1', 'dummy2'), ('dummy0', 'dummy2')),
            ('foo', 'can')
        )

    def test_three_vals(self):
        self.assertEqual(
            pick_col(('foo', 'bar', 'can'), ('dummy0', 'dummy1', 'dummy2'), ('dummy0', 'dummy1', 'dummy2')),
            ('foo', 'bar', 'can')
        )


if __name__ == '__main__':
    unittest_main()
