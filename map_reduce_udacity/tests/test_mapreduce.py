from unittest import TestCase, main as unittest_main
from collections import Counter
from operator import itemgetter

from map_reduce_udacity.experiments.total_sales_all_stores import total_sales_all_stores
from map_reduce_udacity.experiments.max_per_store import max_per_store
from map_reduce_udacity.experiments.total_sales_per_category import total_sales_per_category
from map_reduce_udacity.experiments.hits_to_the_associates_js import hits_to_the_associates_js
from map_reduce_udacity.experiments.hits_to_the_ip import hits_to_the_ip
from map_reduce_udacity.experiments.most_popular_asset import most_popular_asset
from map_reduce_udacity.experiments.student_times import student_times


class TestMapReduce(TestCase):
    def test_on_sales_per_category(self):
        mr_result = total_sales_per_category(print_res=False, filename='head_50_purchases.txt')
        self.assertEqual(mr_result, Counter({'Consumer Electronics': 837.69, 'Toys': 319.72}))

    def test_on_max_per_store(self):
        mr_result = max_per_store(print_res=False, filename='head_50_purchases.txt')
        self.assertEqual(mr_result, {'Chandler': 414.08, 'Reno': 88.25})

    def test_on_total_sales_all_stores(self):
        mr_result = total_sales_all_stores(print_res=False, filename='head_50_purchases.txt')
        self.assertEqual(mr_result, {'price': 11259.82, 'count': 50})

    def test_hits_to_the_associates_js(self):
        mr_result = hits_to_the_associates_js(print_res=False, filename='head_50_access_log')
        self.assertEqual(mr_result, {'the-associates.js': 3})

    def test_hits_to_the_ip(self):
        mr_result = hits_to_the_ip(print_res=False, filename='head_50_access_log')
        self.assertEqual(mr_result, {})

    def test_most_popular_asset(self):
        mr_result = most_popular_asset(print_res=False, filename='access_log')
        print 'mr_result.most_common(10) =', mr_result.most_common(10)
        self.assertEqual(mr_result.most_common(10), [('/', 4)])

    def test_student_times(self):
        # mr_result = student_times(print_res=False, filename='student_test_posts.csv')
        mr_result = student_times(print_res=False, filename='forum_node.tsv')
        # self.assertEqual(mr_result, {'the-associates.js': 3})


if __name__ == '__main__':
    unittest_main()
