from unittest import TestCase, main as unittest_main
from collections import Counter


class TestReduce(TestCase):
    def test_reducer(self):
        res = reduce(counter, ['Bar\t500', 'Bar\t600', 'Can\t300', 'Foo\t300'], Counter())
        self.assertEqual(res, Counter({'Bar': 1100, 'Foo': 300}))


def counter(accumulator, line):
    if line.startswith('Bar') or line.startswith('Foo'):
        l = line.split('\t')
        accumulator[l[0]] += int(l[1])
    return accumulator


if __name__ == '__main__':
    unittest_main()
