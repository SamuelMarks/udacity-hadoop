#!/usr/bin/env python

from fileinput import input as read_input
from collections import OrderedDict
from itertools import chain
from re import findall, compile, match
from datetime import datetime
from pkg_resources import resource_filename
from argparse import ArgumentError, Action
from inspect import currentframe
from pprint import PrettyPrinter

pp = PrettyPrinter(indent=4).pprint

get_lines = lambda args, delim='\t': (lambda line: map(
    lambda l: l.strip().split(delim) if l.find(delim) > -1 else map(
        ''.join, findall(r'\"(.*?)\"|\[(.*?)\]|(\S+)', l)
    ), line
))(args or read_input())

get_headers = lambda lines, headers_included=False, headers=tuple(): (map(
    lambda col_name: col_name.replace('"', ''), lines.pop(0)
) if headers_included else headers)

# E.g.: from: `Counter({'Foo': 5})`, to: (('Foo', '5'),)
to_tuples_of_tuples = lambda dict_like: tuple((key, str(value)) for key, value in dict_like.iteritems()) \
    if hasattr(dict_like, 'iteritems') else dict_like  # Do nothing if no iteritems


def pick_col(row, headers, filters=tuple()):
    """
    :arg row :type enumerable
    :arg headers :type [string]
    :arg filters :type tuple

    :returns the `row` minus any columns you're disinterested in :type tuple
    """

    if not filters:  # Short-circuits. Empty equivalent to * from SQL.
        return row
    res = filter(lambda t: t[0] in filters and t[1],
                 OrderedDict(zip(headers, row)).iteritems())
    if len(res) < 1 or len(res[0]) < 1:
        return tuple()

    return tuple(e[1] for e in res)


def to_tsv(*args):
    """
     :arg each element within has :type which is iterable containing iterables containing strings
         e.g.: to_tsv([['a','b'], ['c','d']]
     :returns Tab-Seperated Values
         e.g.: 'a\tb\nc\td'
    """
    return '\n'.join(map(lambda l: '\t'.join(l), chain(*args)))


retrieve_file = lambda filename: open(resource_filename('map_reduce_udacity.data', filename))


def retrieve_io_object(filename=None, io_object=None):
    if io_object:
        return io_object
    elif not filename:
        raise ArgumentError(Action(['filename', 'io_object'],
                                   currentframe().f_code.co_name), 'One must be included')
    return retrieve_file(filename)


is_datetime_regex = compile(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{6}[\+|\d{2}]*}')
str_is_datetime = lambda date_string: match(is_datetime_regex, date_string)


def valid_date(date_string):
    try:
        mat = match('(\d{2})[/.-](\d{2})[/.-](\d{4})$', date_string)
        if mat is not None:
            datetime(*(map(int, mat.groups()[-1::-1])))
            return True
    except ValueError:
        pass
    return False
