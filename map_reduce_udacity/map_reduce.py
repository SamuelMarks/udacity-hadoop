from StringIO import StringIO

from mappers import mapper
from reducer import reducer
from utils import to_tsv


def map_reduce(data, headers_included=False, headers=tuple(),
               filters=tuple(), reduction_function=None, sequence=None):
    """
    :arg data :type [string]
    :arg headers_included :type bool
    :arg headers :type tuple
    :arg filters :type tuple
    :arg reduction_function :type Callable
    :arg sequence :type iterable
    """
    d = mapper(args=data, filters=filters, headers_included=headers_included, headers=headers)

    return reducer(StringIO(to_tsv(sorted(d.values))), reduction_function, sequence)
