#!/usr/bin/env python

from argparse import ArgumentError, Action
from inspect import currentframe
from collections import namedtuple

from utils import get_headers, get_lines, pick_col, retrieve_file, pp


def mapper(args=None, delim='\t', headers_included=False, headers=tuple(), filters=tuple()):
    """
    :arg args             :type str
        Contains either tab-separated or common-log formatted lines ('\n' newline)
    :arg headers_included :type bool
    :arg headers          :type tuple
    :arg filters          :type tuple

    :returns object with two attributes: values and headers :type namedtuple (Result)
    """
    if not headers_included and not headers:
        raise ArgumentError(Action(['headers_included', 'headers'],
                                   currentframe().f_code.co_name), 'One must be included')
    lines = get_lines(args, delim)
    headers = get_headers(lines, headers_included, headers)

    # for line in lines: pprint(dict(zip(headers, line)), indent=4)

    out = filter(lambda r: len(filter(lambda elem: elem not in ('', None), r)) == len(r),  # Compact
                 map(lambda row: pick_col(row, headers, filters),  # Pick
                     filter(lambda data: len(data) == len(headers), lines)))  # Sanitise
    # pp(out)
    return namedtuple('Result', 'values headers')(values=out, headers=headers)


if __name__ == '__main__':
    '''
    from StringIO import StringIO
    mapper(StringIO("2012-01-01	09:00	San Jose	Men's Clothing	214.05	Amex"))
    '''

    with retrieve_file('head_50_purchases.txt') as f:
        print mapper(f.readlines(), headers=('date', 'time', 'store', 'item', 'cost', 'payment')).values
