#!/usr/bin/env python

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object


def total_sales_all_stores(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'price', 'store'

    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers=('date', 'time', 'store', 'category', 'price', 'card'),
        filters=filter_by_headers, reduction_function=reducer,
        sequence=dict()
    )

    if print_res:
        print '\n', to_tsv(to_tuples_of_tuples(mr_result), )
        # print to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


def reducer(accumulator, line):
    """
       :arg line has :type string
       :accumulator is the store
    """
    price = line[1]
    accumulator['price'] = round(accumulator.get('price', 0) + float(price), 2)
    accumulator['count'] = accumulator.get('count', 0) + 1
    return accumulator

if __name__ == '__main__':
    from sys import stdin

    total_sales_all_stores(io_obj=stdin)
