#!/usr/bin/env python

from map_reduce_udacity.mappers import mapper
from map_reduce_udacity.reducer import reducer
from map_reduce_udacity.utils import retrieve_file, pp

if __name__ == '__main__':
    tsv_content = mapper(retrieve_file('forum_node.tsv'), headers_included=True)

    for search_term in ('fantastic', 'fantastically'):
        fantastic_idx = {int(post[0][1:-1]): post[4].lower().count(search_term)
                         for post in tsv_content.values
                         if search_term in post[4].lower()}
        print '"{0}" appears {1} times'.format(search_term, sum(fantastic_idx.values()))
        print {'On these indices': sorted(fantastic_idx.keys())}
