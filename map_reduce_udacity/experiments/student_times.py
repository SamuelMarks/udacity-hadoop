#!/usr/bin/env python

from operator import itemgetter
from collections import defaultdict, OrderedDict
from datetime import datetime

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object, str_is_datetime

str_to_dt_obj = lambda dt: datetime.strptime(dt[:dt.rfind('+')].strip('"'), '%Y-%m-%d %H:%M:%S.%f')


def student_times(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'author_id', 'added_at'

    '''
    headers = ('id', 'title', 'tagnames', 'author_id', 'body', 'node_type', 'parent_id',
               'abs_parent_id', 'added_at', 'score', 'state_string', 'last_edited_id',
               'last_activity_by_id', 'last_activity_at', 'active_revision_id', 'extra',
               'extra_ref_id', 'extra_count', 'marked')
    '''

    '''
    mr_result = OrderedDict(sorted(map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers_included=True, sequence=defaultdict(list),
        filters=filter_by_headers, reduction_function=reducer
    ).items(), key=lambda v: v[1][0], reverse=True))
    '''
    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers_included=True, sequence=defaultdict(list),
        filters=filter_by_headers, reduction_function=reducer
    )
    map(lambda lst: mr_result[lst].sort(key=itemgetter(0)), mr_result)
    # print tuple(map(lambda l: l[0], mr_result[k]) for k in mr_result)  # Print just the epochs in the lists

    if print_res:
        print to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


def reducer(accumulator, line):
    """
       :arg line has :type string
       :accumulator is the store
    """
    # default_result = ''
    # accumulator.get(line[0], (0,))[0] + 1,

    if not line or not len(line) > 1:
        return accumulator

    dt = str_is_datetime(line[1].strip('"'))
    print 'line[0] =', line[0]
    print
    print 'dttt =', dt
    if dt is not None:
        print 'dt =', dt

    if line and len(line) > 1 and line[0] != '#' and line[1] != '#' and dt is not None:
        print 'line[0] =', line[0]
        print 'line[1] =', line[1]
        accumulator[line[0]].append(
            (int(round((str_to_dt_obj(dt.group()) - datetime(1970, 1, 1)).total_seconds())), line[1]),
        )
    return accumulator


if __name__ == '__main__':
    from sys import stdin

    student_times(io_obj=stdin)
