#!/usr/bin/env python

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object


def hits_to_the_ip(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'ip', 'status'

    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers=('ip', 'identity', 'username', 'timestamp', 'request', 'status', 'bytes'),
        filters=filter_by_headers, reduction_function=reducer, sequence=dict()
    )

    if print_res:
        print '\n', to_tsv(to_tuples_of_tuples(mr_result), )
        # print to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


def reducer(accumulator, line):
    """
       :arg line has :type string
       :accumulator is the store
    """
    ip = '10.99.99.186'
    if line[0] == ip:
        accumulator[ip] = accumulator.get(ip, 0) + 1
    return accumulator


if __name__ == '__main__':
    from sys import stdin

    hits_to_the_ip(io_obj=stdin)
