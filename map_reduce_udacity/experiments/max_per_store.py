#!/usr/bin/env python

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object


def max_per_store(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'store', 'price'

    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers=('date', 'time', 'store', 'category', 'price', 'card'),
        filters=filter_by_headers, reduction_function=reducer, sequence=dict()
    )

    if print_res:
        print '\n', to_tsv(to_tuples_of_tuples(mr_result), )
        # print to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


def reducer(accumulator, line):
    """
       :arg line has :type string
       :accumulator is the store
    """
    store, price = line
    if store in ('Reno', 'Toledo', 'Chandler'):
        accumulator[store] = max(accumulator.get(store, 0), float(price))
    return accumulator


if __name__ == '__main__':
    from sys import stdin

    max_per_store(io_obj=stdin)
