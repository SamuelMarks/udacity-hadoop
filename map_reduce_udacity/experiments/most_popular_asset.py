#!/usr/bin/env python

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object


def most_popular_asset(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'request'

    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers=('ip', 'identity', 'username', 'timestamp', 'request', 'status', 'bytes'),
        filters=filter_by_headers, reduction_function=reducer
    )

    if print_res:
        # print '\n', to_tsv(to_tuples_of_tuples(mr_result), )
        print '\n', to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


from urlparse import urlparse


def reducer(accumulator, line):
    """
       :arg line has :type string
       :accumulator is the store
    """
    key = urlparse(line[1]).path if line[1].startswith('http:') \
        else line[1][line[1].find('/', 7)] if len(line[1]) > 2 else line[1]
    if len(key) > 1:
        accumulator[key] += 1
    return accumulator


if __name__ == '__main__':
    from sys import stdin

    most_popular_asset(io_obj=stdin)
