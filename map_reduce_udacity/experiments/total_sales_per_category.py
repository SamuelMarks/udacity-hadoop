#!/usr/bin/env python

from map_reduce_udacity.map_reduce import map_reduce
from map_reduce_udacity.utils import to_tsv, to_tuples_of_tuples, retrieve_io_object


def total_sales_per_category(filename=None, io_obj=None, print_res=True):
    filter_by_headers = 'category', 'price'

    mr_result = map_reduce(
        data=retrieve_io_object(filename, io_obj),
        headers=('date', 'time', 'store', 'category', 'price', 'card'),
        filters=filter_by_headers, reduction_function=reducer)

    if print_res:
        print '\n', to_tsv(to_tuples_of_tuples(mr_result), )
        # print to_tsv((tuple(filter_by_headers),), to_tuples_of_tuples(mr_result))  # Print with headers

    return mr_result


def reducer(accumulator, line):
    """
       :arg line has :type string
          E.g.: line = ['Toys', '500']
       :accumulator is the store
    """
    category, sales = line
    if category.startswith('Toys') or category.startswith('Consumer'):
        accumulator[category] = round(accumulator[category] + float(sales), 2)
    return accumulator

if __name__ == '__main__':
    from sys import stdin

    total_sales_per_category(io_obj=stdin)
