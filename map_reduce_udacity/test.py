#!/usr/bin/env python


def pick0(r, k):
    Bar, Foo = r
    return locals().get(k)


def pick1(r, k, h):
    try:
        return r[h.index(k)]
    except ValueError:
        return None


pick2 = lambda r, k, h: dict(zip(h, r)).get(k)


def print_f(s):
    print s


pick3 = lambda r, ks, h: map(lambda t: t[0] in ks and t[1] or None,
                             dict(zip(h, r)).iteritems())

if __name__ == '__main__':
    '''
    |-------|
    |Bar|Foo|
    |-------|
    | 5 | 6 |
    |'f'|'g'|
    | 1 | 8 |
    |_______|
    '''

    key = 'Foo'
    headers = 'Bar', 'Foo'
    for row in ((5, 6), ('f', 'g'), (1, 8)):
        # Method 0
        print {'Bar': row[0], 'Foo': row[1]}.get(key)

        # Method 1
        print pick0(row, key)

        # print Foo

        # Method 2
        print pick1(row, key, headers)

        # Method 3
        print pick2(row, key, headers)

        # Method 4
        print pick3(row, ('Foo', 'Bar'), headers)
        print pick3(row, key, headers)
