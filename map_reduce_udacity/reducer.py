#!/usr/bin/env python

from collections import Counter

from utils import get_lines
from types import NoneType


def reducer(args=None, reduction_function=None, sequence=None):
    lines = get_lines(args)

    if not reduction_function:
        return lines

    return reduce(reduction_function, lines, Counter() if type(sequence) is NoneType else sequence)


if __name__ == '__main__':
    pass
